# Quer ser desenvolvedor frontend na Webjump?
Sim, adoraria aceitar esse pedido WebJump, você realizaria o meu sonho de morar em São Paulo, trabalhando com que eu amo, e ainda melhor trabalhando em uma empresa extremamente maravilhosa tenho notado você por favor note meu potencial, prometo não decepcionar você... rsrsr ;)


Gostaria de dizer que apesar de não concluído o desafio na sua totalidade eu aprendi bastante gostaria de agradecer a oportunidade e dizer que ainda estarei usando o desafio como base para aprendizado espero em breve ter concluído 100% do mesmo.

## O que foi feito

Neste desafio decidir não usar qualquer framework ou biblioteca para que eu pudesse mostrar 100% do conhecimento que tenho tanto na parte de estilização usando o CSS juntamente com o JS.

Usei  o elemento de grid do próprio HTML para fazer o layout fluido o que deu um pouco de trabalho pois estava acostumado a usar uma biblioteca chamada minimalize.

Utilizei o fecth para buscar a API.

## Agradecimento

Para rodar o APP basta dar um npm start e ser feliz, espero que o resultado seja o que esteja em busca e caso não seja peço por gentileza alguns comentario no que posso melhorar, ou o que devo estudar para alcançar um conhecimento maior sobre o assunto, desde já agradeço.
